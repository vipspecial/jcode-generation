### 简介
>- 后台程序工具化，连接mysql数据库一键生成规范统一的增删改查操作
>- 包括鉴权、异常处理、接口规范返回、生成表的所有增删该查
>- 生成框架：Spring MVC + Spring Boot 1.5 + Gradle + Mybatis + MySQL + Swagger2
>- 支持二次开发
### 项目采用框架
>- Spring MVC、Spring Boot、Gradle、freemarkTemplate、Swagger2、MySQL
### TODOLIST

- [ ] 前端代码自动集成
- [ ] redis集群支持
- [ ] 鉴权集成redis
- [ ] 定时任务自定义（低优）
- [ ] 调整ID字段兼容String和Long类型
- [X] 1.0版本 

### 项目初始化
##### 前期准备
>- 数据库创建表（做好字段备注）
>- 每个表内字段最好有ID字段，目前只支持String类型
##### 使用工具
>- 1.启动项目
>- 2.访问 localhost:9090
>- 4.默认生成路径 C:/autoJcode

![输入图片说明](https://images.gitee.com/uploads/images/2019/0306/175948_a7d25060_143189.jpeg "4A002184-E9E8-44e7-9473-504DF80E36A2.jpg")
### 项目结构
>项目标识
><br>| - common
><br>| - controller
><br>| - domain
><br>|&ensp;&ensp;| - dto
><br>| - mapper
><br>| - services
><br>|&ensp;&ensp;| - impl
><br>resources
><br>| - application.yml
><br>| - application-dev.yml
><br>| - application-prod.yml
><br>| - logback-spring.xml
><br>build.gradle

### 生成后效果图
![输入图片说明](https://images.gitee.com/uploads/images/2019/0307/103533_e9b94c37_143189.jpeg "DFB7FFCA-542A-410c-99AD-7F3E2BF08B61.jpg")
### 有不足，请多指教