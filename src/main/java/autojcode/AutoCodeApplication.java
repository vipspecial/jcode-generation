package autojcode;

import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by lizhenyu on 2018/5/11
 */
//@EnableSwagger2
@SpringBootApplication
public class AutoCodeApplication {
    public static void main(String[] args) {
        SpringApplication.run(AutoCodeApplication.class, args);
    }
}
