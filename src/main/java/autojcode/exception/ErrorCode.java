package autojcode.exception;

import java.util.Arrays;
import java.util.Optional;

/**
 * 自定义异常
 */
public enum ErrorCode {
    /*成功*/
    OK(200, "成功"),

    GENERIC_ERROR(-1, "一般错误"),



    /*****
     * 业务错误: 400xxxyyy
     * 400：  HTTP状态码
     * xxx: 系统模块编号
     * yyy: 模块内错误编号
     ******/
    BIZ_ERROR(400, "业务错误");

    private final int value;
    private final String msg;

    private ErrorCode(int value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public int value() {
        return this.value;
    }

    public String msg() {
        return this.msg;
    }

    public static ErrorCode valueOf(int value) {
        Optional<ErrorCode> optional = Arrays.stream(values()).filter(instance -> instance.value == value).limit(1).findFirst();
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new IllegalArgumentException("No matching constant for [" + value + "]");
    }
}
