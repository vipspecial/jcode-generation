package autojcode.enums;

/**
 * 数据库枚举
 */
public enum DataBaseEnum {
    MYSQL("MYSQL","com.mysql.cj.jdbc.Driver","jdbc:mysql://%s/%s?useUnicode=true&characterEncoding=utf8&useSSL=false");
    //ORACLE("ORACLE","oracle.jdbc.driver.OracleDriver","","DTO.java");

    /**
     * 名称
     */
    private String name;
    /**
     * 驱动
     */
    private String driver;
    /**
     * 链接路径
     */
    private String url;

    private DataBaseEnum(String name,String driver,String url) {
        this.name = name;
        this.driver = driver;
        this.url=url;
    }

    public String getName() {
        return name;
    }

    public String getDriver() {
        return driver;
    }

    public String getUrl() {
        return url;
    }
}
