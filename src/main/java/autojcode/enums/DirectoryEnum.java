package autojcode.enums;

import freemarker.template.utility.DateUtil;

/**
 * 目录枚举
 */
public enum DirectoryEnum {

    //相对路径位置，模板名称，生成文件前缀，生成文件后缀
    DOMAIN("src/main/java/%s/domain/","Model.ftl","",".java"),
    DOMAIN_DTO("src/main/java/%s/domain/dto/","DTO.ftl","","DTO.java"),
    MAPPER("src/main/java/%s/mapper/","Mapper.ftl","","Mapper.java"),
    MAPPER_PROVIDER("src/main/java/%s/mapper/privider/","MapperProvider.ftl","","Provider.java"),
    CONTROLLER("src/main/java/%s/controller/","Controller.ftl","","Controller.java"),
    SERVICES("src/main/java/%s/services/","ServiceInterface.ftl","I","Service.java"),
    SERVICES_IMPL("src/main/java/%s/services/impl/","ServiceImpl.ftl","","ServiceImpl.java"),

    RUN_APPLICATION("src/main/java/%s/","Application.ftl","","Application.java"),
    COMMON("src/main/java/%s/","","",""),
    RESOURCES("src/main/resources/","","",""),

    GRADLE("/","Gradle.ftl","","build.gradle"),
    APPLICATION_CONFIG("src/main/resources/","ApplicationConfig.ftl","","application.yml"),
    APPLICATION_CONFIG_DEV("src/main/resources/","ApplicationConfigDev.ftl","","application-dev.yml"),
    APPLICATION_CONFIG_PROD("src/main/resources/","ApplicationConfigProd.ftl","","application-prod.yml");

    /**
     * 生成路径
     */
    private String path;
    /**
     * 模板名称
     */
    private String template;
    /**
     * 生成文件名前缀
     */
    private String prefix;
    /**
     * 生成文件名后缀
     */
    private String suffix;

    /**
     * 拼接类名称
     * @param directoryEnum
     * @return
     */
    public static String GetFileName(Object className, DirectoryEnum directoryEnum) {
        return directoryEnum.getPrefix()+className+directoryEnum.getSuffix();
    }

    /**
     * 获取类相对路径
     * @param className
     * @param directoryEnum
     * @return
     */
    public static String GetFilePath(Object className, DirectoryEnum directoryEnum) {
        return directoryEnum.getPath();
    }

    private DirectoryEnum(String path,String template,String prefix,String suffix) {
        this.path = path;
        this.template=template;
        this.prefix=prefix;
        this.suffix=suffix;
    }

    public String getPath() {
        return path;
    }

    public String getTemplate() {
        return template;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }
}
