package autojcode.domain;
import java.io.Serializable;

public class JsonResult<T>  implements Serializable {
    private static final long serialVersionUID = -884815171881944928L;
    private static final int SUCCESS = 200;
    private static final int ERROR = 1;
    private static final String MESSAGE = "成功";

    private int code;
    private String message ="";
    private T data;

   ///构造方法
    public JsonResult() {
        code = SUCCESS;
        message = MESSAGE;
    }
    public JsonResult(T data){
        code = SUCCESS;
        this.data =  data;
    }
    public JsonResult(Throwable e){
        code = ERROR;
        this.message = e.getMessage();
    }


    @Override
    public String toString() {
        return "JsonResult [code=" + code + ", message=" + message + ", data=" + data + "]";
    }

    public static int getSUCCESS() {
        return SUCCESS;
    }

    public static int getERROR() {
        return ERROR;
    }

    public static String getMESSAGE() {
        return MESSAGE;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int state) {
        this.code = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
