package autojcode.domain;

/**
 * 数据库字段封装类
 * Created by Ay on 2017/5/3.
 */
public class TableColumn {

    /** 数据库字段名称 **/
    private String columnName;
    /** 数据库字段类型 **/
    private String columnType;

    /** 数据库字段首字母大写且去掉下划线字符串 **/
    private String columnNameHumpUpper;

    /** 数据库字段首字母小写且去掉下划线字符串 **/
    private String columnNameHump;

    /** 数据库字段注释 **/
    private String columnComment;

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnNameHump() {
        return columnNameHump;
    }

    public void setColumnNameHump(String columnNameHump) {
        this.columnNameHump = columnNameHump;
    }

    public String getColumnNameHumpUpper() {
        return columnNameHumpUpper;
    }

    public void setColumnNameHumpUpper(String columnNameHumpUpper) {
        this.columnNameHumpUpper = columnNameHumpUpper;
    }
}