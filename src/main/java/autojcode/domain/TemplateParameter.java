package autojcode.domain;

import autojcode.enums.DirectoryEnum;

import java.util.List;
import java.util.Set;

/**
 * 模板参数实体
 */
public class TemplateParameter {
    /**
     * 项目主目录
     */
    private String projectDirctory;

    /** 生成文件类型枚举 */
    private DirectoryEnum directoryEnum;
    /** 数据库连接 */
    private String dbUrl;
    /** 数据库用户名 */
    private String dbUserName;
    /** 数据库密码 */
    private String dbPassword;
    /** 原始表名 来源：输入或数据库 */
    private String tableNameOriginal;
    /** 驼峰表名 首字母小写*/
    private String tableNameHump;
    /** 类名定义 首字母大写*/
    private String tableNameHump4Class;
    /** 表描述 来源：数据库 */
    private String tableDescription;
    /** 包名 */
    private String packageName;
    /** 引入依赖 */
    private Set importRelyList;
    /** 作者 */
    private String author;
    /** 文件生成时间 */
    private String generateDate;
    /** 版本 */
    private String version = "1.0.0";

    /** 表字段信息 */
    private List<TableColumn> tableColumnList;

    public String getProjectDirctory() {
        return projectDirctory;
    }

    public void setProjectDirctory(String projectDirctory) {
        this.projectDirctory = projectDirctory;
    }

    public String getTableNameOriginal() {
        return tableNameOriginal;
    }

    public void setTableNameOriginal(String tableNameOriginal) {
        this.tableNameOriginal = tableNameOriginal;
    }

    public String getTableNameHump() {
        return tableNameHump;
    }

    public void setTableNameHump(String tableNameHump) {
        this.tableNameHump = tableNameHump;
    }

    public String getTableNameHump4Class() {
        return tableNameHump4Class;
    }

    public void setTableNameHump4Class(String tableNameHump4Class) {
        this.tableNameHump4Class = tableNameHump4Class;
    }

    public String getTableDescription() {
        return tableDescription;
    }

    public void setTableDescription(String tableDescription) {
        this.tableDescription = tableDescription;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenerateDate() {
        return generateDate;
    }

    public void setGenerateDate(String generateDate) {
        this.generateDate = generateDate;
    }

    public List<TableColumn> getTableColumnList() {
        return tableColumnList;
    }

    public void setTableColumnList(List<TableColumn> tableColumnList) {
        this.tableColumnList = tableColumnList;
    }

    public DirectoryEnum getDirectoryEnum() {
        return directoryEnum;
    }

    public void setDirectoryEnum(DirectoryEnum directoryEnum) {
        this.directoryEnum = directoryEnum;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Set getImportRelyList() {
        return importRelyList;
    }

    public void setImportRelyList(Set importRelyList) {
        this.importRelyList = importRelyList;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }
}
