package autojcode.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 自动生成代码参数
 */
@ApiModel(value = "自动生成代码参数")
public class JcodeProperties implements Serializable {

  /*  @ApiModelProperty(value = "项目名称",required = true)
    private String projectName;

    @ApiModelProperty(value = "包名",required = true,example = "com.example.first-project")
    private String packageName;*/

    @ApiModelProperty(value = "组织",required = true,example = "com.example")
    private String group;

    @ApiModelProperty(value = "项目的唯一的标识符",required = true,example = "first-project")
    private String artifact;

    @ApiModelProperty(value = "作者" ,example = "Z先生")
    private String author="Z先生";

    @ApiModelProperty(value = "版本",name="version",example = "1.0.0")
    private String version= "1.0.0";

    @ApiModelProperty(value = "表名(不填写默认所有表，多表逗号分隔)",example = "表名(不填写默认所有表，多表逗号分隔)")
    private String tableName;

    @ApiModelProperty(value = "生成路径",example = "C:/autoJcode/")
    private String diskPath = "C:/autoJcode/";



    @ApiModelProperty(value = "数据库链接信息",required = true)
    private Db db=new Db();

    public class Db{
       @ApiModelProperty(value = "IP或域名地址",required = true,example = "localhost")
       private String ip = "localhost";

       @ApiModelProperty(value = "端口号",example = "3306")
       private String port = "3306";

       @ApiModelProperty(value = "数据库名称",required = true,example = "数据库名称")
       private String dataBaseName;

       @ApiModelProperty(value = "用户名(必填)",required = true,example = "用户名")
        private String user;

       @ApiModelProperty(value = "密码(必填)",required = true,example = "密码")
        private String password;

       @ApiModelProperty(value = "驱动(非必填)")
        private String driver;

       public String getPort() {
           return port;
       }

       public void setPort(String port) {
           this.port = port;
       }

       public String getIp() {
           return ip;
       }

       public void setIp(String ip) {
           this.ip = ip;
       }

       public String getDataBaseName() {
            return dataBaseName;
        }

        public void setDataBaseName(String dataBaseName) {
            this.dataBaseName = dataBaseName;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    public String getDiskPath() {
        return diskPath;
    }

    public void setDiskPath(String diskPath) {
        this.diskPath = diskPath;
    }

    public Db getDb() {
        return db;
    }

    public void setDb(Db db) {
        this.db = db;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getArtifact() {
        return artifact;
    }

    public void setArtifact(String artifact) {
        this.artifact = artifact;
    }
}
