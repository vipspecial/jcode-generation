package autojcode.domain;

import autojcode.exception.MyBizException;
import org.springframework.http.HttpStatus;

public class ResultUtil {
    /**
     * 返回成功，传入返回体具体出參
     * @param object
     * @return
     */
    public static JsonResult success(Object object){
        JsonResult result = new JsonResult();
        result.setCode(HttpStatus.OK.value());
        result.setMessage("success");
        result.setData(object);
        return result;
    }

    /**
     * 提供给部分不需要出參的接口
     * @return
     */
    public static JsonResult success(){
        return success(null);
    }

    /**
     * 自定义错误信息
     * @param code
     * @param msg
     * @return
     */
    public static JsonResult error(Integer code,String msg){
        JsonResult result = new JsonResult();
        result.setCode(code);
        result.setMessage(msg);
        result.setData(null);
        return result;
    }


    /**
     * 返回异常信息，在已知的范围内
     * @param myBizException
     * @return
     */
    public static JsonResult error(MyBizException myBizException){
        JsonResult result = new JsonResult();
        result.setCode(myBizException.getCode());
        result.setMessage(myBizException.getMessage());
        result.setData(null);
        return result;
    }

    public static JsonResult error(Exception e){
        JsonResult result = new JsonResult();
        result.setCode(-1);
        result.setMessage("系统错误");
        result.setData(null);
        return result;
    }

}
