package autojcode.controller;

import autojcode.domain.JcodeProperties;
import autojcode.domain.JsonResult;
import autojcode.domain.ResultUtil;
import autojcode.exception.MyBizException;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lizhenyu on 2018/8/14
 */
@Api(value = "自动生成代码", description = "自动生成代码")
@RestController
public class JcodeController {
    @Autowired
    private CodeGenerateUtils codeGenerateUtils;

    @ApiOperation(value = "自动生成代码", notes = "自动生成代码", position = 1)
    @PostMapping(value = "/autoJCode")
    public JsonResult autoJcodeForDb(@RequestBody @ApiParam(name = "jcodeProperties", value = "生成参数", required = true) JcodeProperties jcodeProperties) {
        try {
            String path = codeGenerateUtils.autoJcodeForDb(jcodeProperties);
            return ResultUtil.success(path);
        } catch (MyBizException e) {
            return ResultUtil.error(e);
        }
    }

    @ApiOperation(value = "自动生成代码", notes = "自动生成代码", position = 1)
    @PostMapping(value = "/dbTest")
    public JsonResult dbTest(@RequestBody JcodeProperties properties) {
        try {
            return ResultUtil.success(codeGenerateUtils.getDbTest(properties));
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }
}
