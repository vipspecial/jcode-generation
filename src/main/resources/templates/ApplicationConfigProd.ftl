server:
  port: 80
spring:
  #MySql
  datasource:
      url: ${dbUrl}
      username: ${dbUserName}
      password: ${dbPassword}