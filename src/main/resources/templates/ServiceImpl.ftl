package ${packageName}.services.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import ${packageName}.domain.dto.${tableNameHump4Class}DTO;
import ${packageName}.domain.${tableNameHump4Class};
import ${packageName}.mapper.${tableNameHump4Class}Mapper;
import ${packageName}.services.I${tableNameHump4Class}Service;
import ${packageName}.common.exception.MyBizException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
* 描述：${tableDescription?default(tableNameHump4Class)} 服务实现层
* @author ${author!}
* @date ${generateDate}
* @version ${version}
*/
@Service
public class ${tableNameHump4Class}ServiceImpl implements I${tableNameHump4Class}Service {
<#--extends BaseServiceImpl<${tableNameHump4Class}, String> -->
    private static final Logger logger = LoggerFactory.getLogger(${tableNameHump4Class}ServiceImpl.class);

    @Autowired
    private ${tableNameHump4Class}Mapper ${tableNameHump}Mapper;

    /**
     * 分页查询${tableDescription?default(tableNameHump4Class)}列表
     * @param pageNum
     * @param pageSize
     * @param ${tableNameHump}
     * @return PageInfo<${tableNameHump4Class}DTO>
     */
    @Override
    public PageInfo<${tableNameHump4Class}DTO> findPageList(int pageNum, int pageSize, ${tableNameHump4Class} ${tableNameHump}) {
        //使用分页插件,核心代码就这一行
        PageHelper.startPage(pageNum, pageSize);
        List<${tableNameHump4Class}DTO> list = ${tableNameHump}Mapper.findList(${tableNameHump});
        PageInfo<${tableNameHump4Class}DTO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
    * 根据ID获取${tableDescription?default(tableNameHump4Class)}信息
    * @param id
    * @return ${tableNameHump4Class}DTO
    */
    @Override
    public ${tableNameHump4Class}DTO findById(String id){
        ${tableNameHump4Class}DTO ${tableNameHump}DTO = new ${tableNameHump4Class}DTO();
        ${tableNameHump4Class} ${tableNameHump} = ${tableNameHump}Mapper.findById(id);
        BeanUtils.copyProperties(${tableNameHump},${tableNameHump}DTO);
        return ${tableNameHump}DTO;
    }

    /**
    * 保存${tableDescription?default(tableNameHump4Class)}
    * @param ${tableNameHump} ${tableDescription?default(tableNameHump4Class)}实体
    * @return ID
    */
    @Override
    public String save(${tableNameHump4Class} ${tableNameHump}){
        int count = ${tableNameHump}Mapper.save(${tableNameHump});
        return ${tableNameHump}.getId();
    }

    /**
    * 更新${tableDescription?default(tableNameHump4Class)}
    * @param ${tableNameHump} ${tableDescription?default(tableNameHump4Class)}实体
    * @return true or false
    */
    @Override
    public boolean update(${tableNameHump4Class} ${tableNameHump}) throws MyBizException {
       //查询是否存在
       ${tableNameHump4Class} model = findById(${tableNameHump}.getId());
       if (model == null) {
            throw new MyBizException(-1, "当前数据不存在");
       }
       ${tableNameHump}Mapper.update(${tableNameHump});
       return true;
    }
    /**
    * 删除${tableDescription?default(tableNameHump4Class)}
    * @param id 删除ID
    * @return true or false
    */
    @Override
    public boolean delete(String id) throws MyBizException {
       //查询是否存在
        ${tableNameHump4Class} ${tableNameHump} = findById(id);
       if (${tableNameHump} == null) {
          throw new MyBizException(-1, "当前数据不存在");
       }
       ${tableNameHump}Mapper.delete(id);
       return true;
    }
}