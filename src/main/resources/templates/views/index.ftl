<!DOCTYPE html>
<html lang="zh-CN" xmlns="http://www.w3.org/1999/html">
<head lang="en">
    <title>Java+Vue前后端分离-工程自动生成神器</title>
    <link href="../css/index.css" rel="stylesheet">

    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="../js/index.js"></script>
</head>
<body>
<div class="container">
    <div class="row show-grid">
        <div class="col-xs-2 col-md-2">
            <div class="row show-grid">
                <div class="col-xs-6">
                    <h4>配置<h4>
                </div>
            </div>

            <div class="row show-grid">
                <div id="savedConf"></div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">

            <div class="row show-grid">
                <div class="col-xs-6">
                    <h4>项目<h4>
                </div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">group</span>
                        <input value="com.example" type="text" id="group" class="form-control" placeholder="com.example"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">artifact</span>
                        <input value="first-project" type="text" id="artifact" class="form-control" placeholder="first-project"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">author</span>
                        <input type="text" id="author" class="form-control" placeholder="作者" value="Z先生"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">version</span>
                        <input type="text" id="version" class="form-control" value="1.0.0" placeholder="V1.0.0"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">
                    <h4>数据库<h4>
                </div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">地址</span>
                        <input type="text" id="address" class="form-control" placeholder="IP:端口号"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">数据库</span>
                        <input type="text" id="dataBaseName" class="form-control" placeholder="数据库"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">user</span>
                        <input type="text" id="dbUser" class="form-control" placeholder="username"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">password</span>
                        <input type="password" id="dbPassword" class="form-control" placeholder="password"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">本地位置</span>
                        <input type="text" id="localPath" class="form-control" value="C:/autoJcode"
                               placeholder="C:/autoJcode"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">
                    <h4>操作<h4>
                </div>
            </div>
            <div class="row show-grid">
                <div class="col-xs-6">
                    <div class="input-group">
                        <button type="button" id="jcodeAction" class="btn btn-primary">生成工程</button>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="input-group">
                        <input type="text" maxlength="10" id="confName" class="form-control" placeholder="例如：测试环境"
                               aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="saveConfig">保存配置</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>