package ${packageName}.domain;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
<#if importRelyList?exists>
    <#list importRelyList as model>
import ${model!};
    </#list>
</#if>

/**
* 描述：${tableDescription?default(tableNameHump4Class)}实体
* @author ${author!}
* @date ${generateDate}
* @version ${version}
*/
@ApiModel(value = "${tableDescription?default(tableNameHump4Class)}")
public class ${tableNameHump4Class} implements Serializable {
<#if tableColumnList?exists>
    <#list tableColumnList as model>
    <#--/**${model.columnComment!}*/-->

    @ApiModelProperty(value = "${model.columnComment!}")
   <#-- @Column(name = "${domain.columnName}",columnDefinition = "${domain.columnType}")-->
    private ${model.columnType} ${model.columnNameHump};
    </#list>
</#if>

    /******* getter and setter *******/
<#if tableColumnList?exists>
    <#list tableColumnList as model>
    public ${model.columnType} get${model.columnNameHumpUpper}() {
        return this.${model.columnNameHump};
    }
    public void set${model.columnNameHumpUpper}(${model.columnType} ${model.columnNameHump}) {
        this.${model.columnNameHump} = ${model.columnNameHump};
    }
    </#list>
</#if>

}