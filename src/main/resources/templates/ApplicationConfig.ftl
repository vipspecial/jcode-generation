server:
  port: 8080
spring:
  profiles:
    #环境
    active: dev
  #MySql
  datasource:
      url: ${dbUrl}
      username: ${dbUserName}
      password: ${dbPassword}