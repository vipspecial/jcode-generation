package ${packageName}.services;

import ${packageName}.common.exception.MyBizException;
import ${packageName}.domain.${tableNameHump4Class};
import ${packageName}.domain.dto.${tableNameHump4Class}DTO;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

/**
* 描述：${tableDescription?default(tableNameHump4Class)} 服务接口
* @author ${author!}
* @date ${generateDate}
* @version ${version}
*/
public interface I${tableNameHump4Class}Service {
<#--extends IBaseService<${tableNameHump4Class},String>-->

    PageInfo<${tableNameHump4Class}DTO> findPageList(int spageNum, int pageSize, ${tableNameHump4Class} ${tableNameHump});

    ${tableNameHump4Class}DTO findById(String id);

    String save(${tableNameHump4Class} ${tableNameHump});

    boolean update(${tableNameHump4Class} ${tableNameHump}) throws MyBizException;

    boolean delete(String id) throws MyBizException;
}