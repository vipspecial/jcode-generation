package ${packageName}.controller;

import com.github.pagehelper.PageInfo;
import ${packageName}.common.exception.MyBizException;
import ${packageName}.common.util.JsonResult;
import ${packageName}.common.util.ResultUtil;
import ${packageName}.domain.dto.${tableNameHump4Class}DTO;
import ${packageName}.domain.${tableNameHump4Class};
import ${packageName}.services.I${tableNameHump4Class}Service;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
* 描述：${tableDescription?default(tableNameHump4Class)}接口
* @author ${author!}
* @date ${generateDate}
* @version ${version}
*/
@Api(description = "${tableDescription?default(tableNameHump4Class)}接口")
@Produces(MediaType.APPLICATION_JSON)
@RestController
@RequestMapping("/${tableNameHump}Api")
public class ${tableNameHump4Class}Controller {
    private static final Logger logger= LoggerFactory.getLogger(${tableNameHump4Class}Controller.class);
    @Autowired
    private I${tableNameHump4Class}Service ${tableNameHump}Service;

    @ApiOperation(value = "根据ID获取${tableDescription?default(tableNameHump4Class)}信息", position = 1)
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
    public JsonResult get${tableNameHump4Class}ById(@ApiParam(name="id",value="唯一ID",required=true) @PathVariable String id){
        ${tableNameHump4Class}DTO ${tableNameHump4Class}DTO = ${tableNameHump}Service.findById(id);
        return ResultUtil.success(${tableNameHump4Class}DTO);
    }

    @ApiOperation(value = "获取${tableDescription?default(tableNameHump4Class)}列表", httpMethod = "POST", position = 2)
    @PostMapping(value = "/${tableNameHump}List", produces = MediaType.APPLICATION_JSON)
    public JsonResult get${tableNameHump4Class}List(@ApiParam(name="pageNum",value="页码",required=true,defaultValue = "1") @RequestParam int pageNum,
                                                       @ApiParam(name="pageSize",value="每页条数",required=true,defaultValue = "10") @RequestParam int pageSize,
                                                       @ApiParam(name="${tableNameHump}",value="${tableDescription?default(tableNameHump4Class)}实体") @RequestBody ${tableNameHump4Class} ${tableNameHump}) {
        PageInfo<${tableNameHump4Class}DTO> list = ${tableNameHump}Service.findPageList(pageNum,pageSize,${tableNameHump});
        return ResultUtil.success(list);
    }

    @ApiOperation(value = "保存${tableDescription?default(tableNameHump4Class)}", httpMethod = "PUT", position = 3)
    @PutMapping(value = "/save${tableNameHump4Class}", produces = MediaType.APPLICATION_JSON)
    public JsonResult save${tableNameHump4Class}(@ApiParam(name="${tableNameHump}",value="${tableDescription?default(tableNameHump4Class)}实体",required=true) @RequestBody ${tableNameHump4Class} ${tableNameHump}) {
        String id= ${tableNameHump}Service.save(${tableNameHump});
        if(ObjectUtils.isEmpty(id)){
            return ResultUtil.error(-1,"保存失败");
        }
        return ResultUtil.success(id);
    }

    @ApiOperation(value = "更新${tableDescription?default(tableNameHump4Class)}", httpMethod = "PUT", position = 4)
    @PutMapping(value = "/update${tableNameHump4Class}", produces = MediaType.APPLICATION_JSON)
    public JsonResult update${tableNameHump4Class}(@ApiParam(name="${tableNameHump}",value="${tableDescription?default(tableNameHump4Class)}实体",required=true) @RequestBody ${tableNameHump4Class} ${tableNameHump}) throws MyBizException {
        Boolean result= ${tableNameHump}Service.update(${tableNameHump});
        if(!result){
             return ResultUtil.error(-1,"更新失败");
        }
        return ResultUtil.success();
    }

    @ApiOperation(value = "删除${tableDescription?default(tableNameHump4Class)}", httpMethod = "DELETE", notes = "删除${tableDescription?default(tableNameHump4Class)}", position = 5)
    @DeleteMapping(value = "/delete${tableNameHump4Class}/id",produces = MediaType.APPLICATION_JSON)
    public JsonResult delete${tableNameHump4Class}(@ApiParam(name="id",value="唯一ID",required=true) @PathVariable String id) throws MyBizException{
        Boolean result= ${tableNameHump}Service.delete(id);
        if(!result){
            return ResultUtil.error(-1,"删除失败");
        }
        return ResultUtil.success();
    }
}