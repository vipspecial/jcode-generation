server:
  port: 8080
spring:
  #MySql
  datasource:
      url: ${dbUrl}
      username: ${dbUserName}
      password: ${dbPassword}