buildscript {
    ext {
        springBootVersion = '1.5.10.RELEASE'
    }
    repositories {
        maven{ url'http://maven.aliyun.com/nexus/content/groups/public/'}
        mavenCentral()
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:<#noparse>${springBootVersion}</#noparse>")
    }
}

apply plugin: 'java'
apply plugin: 'idea'
apply plugin: 'org.springframework.boot'

group = '${packageName}'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = 1.8

repositories {
    mavenCentral()
}

dependencies {
    compile 'org.springframework.boot:spring-boot-starter-web'
    //json
    compile 'com.alibaba:fastjson:1.1.23'
    //pager
    compile 'com.github.pagehelper:pagehelper-spring-boot-starter:1.2.5'
    //热部署
    compile 'org.springframework.boot:spring-boot-devtools'
    compile 'mysql:mysql-connector-java:5.1.40'
    compile 'org.springframework.boot:spring-boot-starter-logging'
    compile 'org.mybatis.spring.boot:mybatis-spring-boot-starter:1.3.0'
    //swagger
    compile 'io.springfox:springfox-swagger2:2.6.1'
    compile 'io.springfox:springfox-swagger-ui:2.6.1'
    //swagger参数
    compile 'javax.ws.rs:javax.ws.rs-api:2.0'
    compile 'org.apache.httpcomponents:httpclient:4.5.5'
    //AOP
    compile 'org.springframework.boot:spring-boot-starter-aop'
    compile 'org.springframework.boot:spring-boot-starter-redis:1.4.7.RELEASE'
    testCompile 'org.springframework.boot:spring-boot-starter-test'
}
