package ${packageName}.mapper;

import ${packageName}.domain.dto.${tableNameHump4Class}DTO;
import ${packageName}.domain.${tableNameHump4Class};
import ${packageName}.mapper.${tableNameHump4Class}Provider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;
import java.util.List;

/**
* 描述：${tableDescription?default(tableNameHump4Class)} 数据持久层
* @author ${author!}
* @date ${generateDate}
* @version ${version}
*/
@Component
@Mapper
public interface ${tableNameHump4Class}Mapper {

    @SelectProvider(method = "find${tableNameHump4Class}List",type = ${tableNameHump4Class}Provider.class)
    public List<${tableNameHump4Class}DTO> findList(${tableNameHump4Class} ${tableNameHump});

    @Select("select * from `${tableNameOriginal}` where id=<#noparse>#{id}</#noparse>")
    public ${tableNameHump4Class} findById(String id);
<#assign columnNames = "" />
    <#assign columnNameHump = "" />
       <#if tableColumnList?exists>
           <#list tableColumnList as model>
               <#if columnNames == "">
                   <#assign columnNames =  model.columnName/>
                   <#assign columnNameHump = model.columnNameHump/>
               <#else>
                   <#assign columnNames = columnNames+","+ model.columnName/>
                   <#assign columnNameHump = columnNameHump+","+model.columnNameHump/>
               </#if>
           </#list>
       </#if>

    @Insert("INSERT INTO `${tableNameOriginal}` (${columnNames})" +
                           "VALUES(${columnNameHump})")
    public int save(${tableNameHump4Class} ${tableNameHump});

    @UpdateProvider(method = "update${tableNameHump4Class}",type = ${tableNameHump4Class}Provider.class)
    public int update(${tableNameHump4Class} ${tableNameHump});

    @Delete("DELETE FROM  `${tableNameOriginal}` WHERE ID=<#noparse>#{id}</#noparse>")
    public int delete(String id);
}
