package ${packageName}.domain.dto;

import ${packageName}.domain.${tableNameHump4Class};

/**
* 描述：${tableDescription?default(tableNameHump4Class)}DTO
* @author ${author!}
* @date ${generateDate}
* @version ${version}
*/
public class ${tableNameHump4Class}DTO extends ${tableNameHump4Class}{

}