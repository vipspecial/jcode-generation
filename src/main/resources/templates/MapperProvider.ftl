package ${packageName}.mapper;

import ${packageName}.domain.dto.${tableNameHump4Class}DTO;
import ${packageName}.domain.${tableNameHump4Class};
import org.apache.ibatis.annotations.Param;
import org.springframework.util.ObjectUtils;

/**
* 描述：${tableDescription?default(tableNameHump4Class)} 数据持久层
* @author ${author!}
* @date ${generateDate}
* @version ${version}
*/
public class ${tableNameHump4Class}Provider {
    /**
    * 组装查询语句
    * @param ${tableNameHump} 实体
    * @return
    */
    public String find${tableNameHump4Class}List(@Param("${tableNameHump}") ${tableNameHump4Class} ${tableNameHump}) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select * from `${tableNameOriginal}` where 1=1 ");
        <#if tableColumnList?exists>
            <#list tableColumnList as model>
                <#if model.columnComment !="">
        /** ${model.columnComment!} */
                </#if>
                <#if model.columnNameHump?contains("Name")>
        if(!ObjectUtils.isEmpty(${tableNameHump}.get${model.columnNameHumpUpper}())){
            sb.append(" and ${model.columnName} like CONCAT('%',<#noparse>#{</#noparse>${model.columnNameHump}<#noparse>}</#noparse>,'%')");
        }
                <#else>
        if(!ObjectUtils.isEmpty(${tableNameHump}.get${model.columnNameHumpUpper}())){
            sb.append(" and ${model.columnName} = <#noparse>#{</#noparse>${model.columnNameHump}<#noparse>}</#noparse>");
        }
                </#if>
            </#list>
        </#if>
        return sb.toString();
    }
    /**
    * 组装更新语句
    * @param ${tableNameHump} 实体
    * @return
    */
    public String update${tableNameHump4Class}(@Param("${tableNameHump}") ${tableNameHump4Class} ${tableNameHump}) {
        StringBuilder sb = new StringBuilder();
        sb.append(" update `${tableNameOriginal}` set ");
        <#if tableColumnList?exists>
            <#list tableColumnList as model>
        /** ${model.columnComment!} */
        if(!ObjectUtils.isEmpty(${tableNameHump}.get${model.columnNameHumpUpper}())){
            sb.append(" ${model.columnName} = <#noparse>#{</#noparse>${model.columnNameHump}<#noparse>}</#noparse> ");
        }
            </#list>
        </#if>
        <#--sb.append(" modify_date ="+  new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()));-->
        sb.append(" where id=<#noparse>${id}</#noparse> ");
        return sb.toString();
    }
}
