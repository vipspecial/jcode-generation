package #{packageName}.common;

import #{packageName}.common.config.RedisKeyConfig;
import #{packageName}.common.template.RedisTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CommonUtil {

    /**
     * 功能：获取UUID
     */
    public static String getUUID() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replaceAll("-", "");
    }

}
