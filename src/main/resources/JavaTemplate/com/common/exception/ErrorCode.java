package #{packageName}.common.exception;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created by lizhenyu on 2018/4/24
 */
public enum ErrorCode {
    /*成功*/
    OK(Response.Status.OK.getStatusCode(), "成功"),

    GENERIC_ERROR(-1, "一般错误"),



    /*****
     * 业务错误: 400xxxyyy
     * 400：  HTTP状态码
     * xxx: 系统模块编号
     * yyy: 模块内错误编号
     ******/
    BIZ_ERROR(Response.Status.BAD_REQUEST.getStatusCode(), "业务错误"),
    /***** 参数相关错误 *****/
    BIZ_ERROR_PARAMETER(400000000, "参数错误"),
    BIZ_ERROR_PARAMETER_NULL(400000001, "空的请求参数"),
    BIZ_ERROR_PARAMETER_INVALID(400000002, "无效的请求参数"),

    /***** 公共服务 *****/
    BIZ_ERROR_CODE_ERROR(400000010, "请输入正确的验证码"),
    BIZ_ERROR_CODE_NOT_EXIST(400000011, "验证码不存在或者已经过期"),
    BIZ_ERROR_TFS_SUFFIX_EMPTY(400000012, "未指定文件后缀名"),
    BIZ_ERROR_TFS_FILE_EMPTY(400000013, "文件未上传"),
    BIZ_ERROR_SMS_ERROR(400000014, "调用发送短信接口失败"),
    BIZ_ERROR_SMS_NOT_ALLOWED(400000015, "此手机号码不允许发送信息"),
    BIZ_ERROR_SMS_MOBILE_ERROR(400000016, "手机号码不正确"),
    BIZ_ERROR_COMMON_NOT_EXIST(400000017, "数据不存在"),
    BIZ_ERROR_COMMON_EXIST(400000018, "数据已存在"),
    BIZ_ERROR_COMMON_ADD(400000019, "新增失败"),
    BIZ_ERROR_COMMON_UPDATE(400000020, "更新失败"),
    BIZ_ERROR_COMMON_DELETE(400000021, "删除失败"),
    BIZ_ERROR_COMMON_SIGN(400000022, "签名校验不通过"),
    /****ticket的异常****/
    BIZ_ERROR_COMMON_TICKET_PARAMETER(400000023, "ticket无效的请求参数"),
    BIZ_ERROR_COMMON_TICKET_AUTH(400000024, "ticket校验异常"),

    /***** 用户 *****/
    BIZ_ERROR_USER_UNKNOWN(400001000, "未知用户错误"),
    BIZ_ERROR_USER_ID_NULL(400001001, "用户ID为空"),
    BIZ_ERROR_USER_NAME_NULL(400001002, "用户名为空"),
    BIZ_ERROR_USER_PWD_NULL(400001003, "用户密码为空"),
    BIZ_ERROR_USER_COUNT_NOT_EXIST(400001004, "用户账号不存在"),
    BIZ_ERROR_USER_COUNT_EXIST(400001005, "用户账号存在"),
    BIZ_ERROR_USER_COUNT_NAME_NOMATHCH(400001006, "用户账号与密码不匹配"),
    BIZ_ERROR_USER_NOT_EXIST(400001007, "用户不存在"),
    BIZ_ERROR_USER_TOKEN_NULL(400001008, "用户TOKEN为空"),
    BIZ_ERROR_USER_ADD_ERROR(400001009, "新增用户失败"),
    BIZ_ERROR_USER_MOBILE_NULL(400001010, "用户手机号为空"),
    BIZ_ERROR_USER_UPDATE_FAIL(400001011, "修改用户失败"),
    BIZ_ERROR_USER_NOT_LOGIN(400001012, "用户未登录"),
    BIZ_ERROR_USER_UPDATE_PWD_FAIL(400001013, "用户密码重置失败"),
    BIZ_ERROR_USER_DELETE_FAIL(400001014, "删除用户失败"),
    BIZ_ERROR_USER_FREEZE_FALI(400001015, "冻结用户失败"),
    BIZ_ERROR_USER_ONSET_FAIL(400001016, "启用用户失败"),
    BIZ_ERROR_USER_EMAIL_NULL(400001017, "用户邮箱为空"),
    BIZ_ERROR_USER_EMAIL_EXIST(400001018, "用户邮箱存在"),
    BIZ_ERROR_USER_EMAIL_NOT_EXIST(400001019, "用户邮箱不存在"),
    BIZ_ERROR_USER_MOBILE_EXIST(400001020, "用户手机号存在"),
    BIZ_ERROR_USER_MOBILE_NOT_EXIST(400001021, "用户手机号不存在"),
    BIZ_ERROR_USER_UPDATE_NAME_FAIL(400001022, "用户名修改失败"),
    BIZ_ERROR_USER_UPDATE_EMAIL_FAIL(400001023, "用户邮箱修改失败"),
    BIZ_ERROR_USER_UPDATE_LOGINTIME_FAIL(400001024, "修改登录时间失败"),
    BIZ_ERROR_USER_UPDATE_LOGINTIMEIP_FAIL(400001025, "修改登录时间与IP失败"),
    BIZ_ERROR_USER_NO_DATA(400001026, "未找到用户数据"),
    BIZ_ERROR_USER_TYPE_NULL(400001027, "用户类别为空"),
    BIZ_ERROR_USER_TYPE_ERROR(400001028, "用户类别错误"),
    BIZ_ERROR_USER_SOURCE_NULL(400001029, "用户来源为空"),
    BIZ_ERROR_USER_STATE_NULL(400001030, "用户状态为空"),
    BIZ_ERROR_USER_MODIFY_TYPE_FAIL(400001031, "用户类别修改失败"),
    BIZ_ERROR_USER_MODIFY_MOBILE_FAIL(400001032, "用户手机号修失败"),
    BIZ_ERROR_USER_ADD_ROLE_ID_EXIST(400001033, "用户ID已存在"),
    BIZ_ERROR_USER_ADD_ROLE_FAIL(400001034, "用户角色添加失败"),
    BIZ_ERROR_USER_NAME_EXIST(400001035, "用户名已存在"),
    BIZ_ERROR_USER_AUTH(400001036, "用户权限不足"),
    BIZ_ERROR_USER_NOT_LOGIN_ADMIN(400001037, "后台用户未登录"),
    BIZ_ERROR_USER_AUTH_ADMIN(400001038, "后台用户权限不足"),
    /**** 用户登录的时候需要验证 ****/
    BIZ_ERROR_USER_LOGIN_VERIFICATION(400001039, "登录需要验证"),
    BIZ_ERROR_USER_LOGIN_VERIFICATION_TIME(400001040, "长时间未登录,需要校验"),
    BIZ_ERROR_USER_LOGIN_VERIFICATION_IP(400001041, "登录IP异常,需要校验"),
    BIZ_ERROR_USER_LOGIN_VERIFICATION_DEVICE(400001042, "切换登录设备,需要检验"),
    BIZ_ERROR_USER_LOGIN_VERIFICATION_ERROR_OVER_TIME(400001043, "失败次数过多,需要检验"),
    BIZ_ERROR_USER_LOGIN_VERIFICATION_ERROR_OVER_TIME_FROZEN(400001044, "失败次数过多,用户已经冻结"),
    BIZ_ERROR_USER_LOGIN_KICK(400001045, "用户在其他设备登录，您已经被踢掉"),


    /**** 订单错误码 ****/
    BIZ_ERROR_ORDER_ID_NULL(500001001, "订单不存在"),
    BIZ_ERROR_ORDER_ADD_FALI(500001002, "下单失败，请稍再试"),
    BIZ_ERROR_ORDER_UPDATE_FALI(500001003, "订单更新失败，请稍再试");



    private final int value;
    private final String msg;

    private ErrorCode(int value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public int value() {
        return this.value;
    }

    public String msg() {
        return this.msg;
    }

    public static ErrorCode valueOf(int value) {
        Optional<ErrorCode> optional = Arrays.stream(values()).filter(instance -> instance.value == value).limit(1).findFirst();
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new IllegalArgumentException("No matching constant for [" + value + "]");
    }
}
