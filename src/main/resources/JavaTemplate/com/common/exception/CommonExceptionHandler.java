package #{packageName}.common.exception;

import #{packageName}.common.util.JsonResult;
import #{packageName}.common.util.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description: 异常统一处理
 * @author: lizy
 * @date: 2019/1/30
 */
@ControllerAdvice
public class CommonExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(CommonExceptionHandler.class);

    /**
     *  拦截Exception类的异常
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public JsonResult exceptionHandler(Exception e){
        logger.error("Error:",e);
        return ResultUtil.error(new MyBizException(ErrorCode.GENERIC_ERROR.value(),ErrorCode.GENERIC_ERROR.msg()));
    }

    /**
     * 拦截捕捉自定义异常 MyBizException.class
     * @param
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = MyBizException.class)
    public JsonResult myErrorHandler(MyBizException e) {
        logger.error("MyError:",e);
        return ResultUtil.error(e);
    }
}
