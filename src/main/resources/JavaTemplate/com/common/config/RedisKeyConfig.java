package #{packageName}.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RedisKeyConfig {
    /**
     * redis key前缀
     */
    @Value("${appApi.redisKey.keyPrefix:prefix}")
    private String keyPrefix;
    /**保持登录时长*/
    @Value("${appApi.loginKeepTime:2000}")
    private long loginKeepTime;

    /**
     * 获取redis Key
     * @param originalId 原始ID
     * @return redis中的实际Key
     */
    public String getRedisKey(String originalId) {
        return keyPrefix+originalId;
    }

    public long getLoginKeepTime() {
        return loginKeepTime;
    }
}
