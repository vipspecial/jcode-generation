package #{packageName}.common;

public class BaseDomain {
    /*登录toke*/
    private String token;
    /*页码*/
    private int pageNum;
    /*每页数*/
    private int pageSize;

    @Override
    public String toString() {
        return "BaseDomain{" +
                "token='" + token + '\'' +
                ", pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                '}';
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
